//
//  BirthdayListCellPresenter.swift
//  BirthdayReminder
//
//  Created by Philipp Otto on 25.06.17.
//  Copyright © 2017 Philipp. All rights reserved.
//

import Foundation

class BirthdayListCellPresenter: MVPTableViewCellPresenter {
    private weak var view: BirthdayListCellViewProtocol?
    
    fileprivate(set) lazy var birthday = Birthday()
    
    // Unfortunately lazy properties do not support didSet
    func setBirthday(_ birthday: Birthday) {
        self.birthday = birthday
        
        self.view?.showBirthday()
    }
    
    override func attachView(_ view: ViewProtocol) {
        guard let view = view as? BirthdayListCellViewProtocol else {
            assertionFailure("The given view does not implement the correct protocol for this presenter")
            return
        }
        
        self.view = view
    }
    
    var daysLeftString: String {
        if self.birthday.daysLeft <= 0 {
            return "Today".localized
        }
        
        return "In \(self.birthday.daysLeft) days"
    }
}
