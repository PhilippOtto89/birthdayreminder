//
//  BirthdayListViewController.swift
//  BirthdayReminder
//
//  Created by Philipp Otto on 18.06.17.
//  Copyright © 2017 Philipp. All rights reserved.
//

import UIKit

class BirthdayListViewController: MVPViewController<BirthdayListPresenter>, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBAction func addTouchUpInside(_ sender: Any) {
        self.presenter.addBirthday()
    }
    
    override func showLoading() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    override func hideLoading() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    // MARK: - UISearchBarDelegate -
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.presenter.filterBirthdays(byName: searchText)
    }
    
    // MARK: - UITableViewDelegate, UITableViewDataSource -
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.birthdays.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "BirthdayListCell") as? BirthdayListTableViewCell else {
            return UITableViewCell()
        }
        
        let birthday = self.presenter.birthdays[indexPath.row]
        
        cell.presenter.setBirthday(birthday)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.presenter.showDetails(forBirthdayAtIndex: indexPath.row)
    }
}

// MARK: - IBirthdayListView -
extension BirthdayListViewController: BirthdayListViewProtocol {
    func showBirthdays() {
        self.tableView.reloadData()
    }
}
