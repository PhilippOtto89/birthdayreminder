//
//  BirthdayListTableViewCell.swift
//  BirthdayReminder
//
//  Created by Philipp Otto on 18.06.17.
//  Copyright © 2017 Philipp. All rights reserved.
//

import UIKit
import AlamofireImage

class BirthdayListTableViewCell: MVPTableViewCell<BirthdayListCellPresenter> {
    
    @IBOutlet weak fileprivate var personImageView: PersonImageView!
    @IBOutlet weak fileprivate var loadImageActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak fileprivate var nameLabel: UILabel!
    @IBOutlet weak fileprivate var ageLabel: UILabel!
    @IBOutlet weak fileprivate var dateLabel: UILabel!
    @IBOutlet weak fileprivate var starSignLabel: UILabel!
    @IBOutlet weak fileprivate var daysLeftLabel: UILabel!
    
    func setImage() {
        guard let imageUrlString = self.presenter.birthday.imageUrl, let imageUrl = URL(string: imageUrlString) else {
            self.personImageView.image = #imageLiteral(resourceName: "ProfileImageStandard")
            return
        }
        
        self.loadImageActivityIndicator.startAnimating()
        
        self.personImageView.af_setImage(withURL: imageUrl, placeholderImage: nil, filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .crossDissolve(0.2), runImageTransitionIfCached: false) { response in
            self.loadImageActivityIndicator.stopAnimating()
        }
    }
    
    override func prepareForReuse() {
        self.personImageView?.image = nil
    }
}

// MARK: - BirthdayListCellViewProtocol -
extension BirthdayListTableViewCell: BirthdayListCellViewProtocol {
    func showBirthday() {
        let birthday = self.presenter.birthday
        
        self.nameLabel.text = birthday.name
        
        self.dateLabel.text = birthday.date.dayMonthLongFormat
        
        self.ageLabel.text = "Age: \(self.presenter.birthday.age)"
        
        self.starSignLabel.text = self.presenter.birthday.starSign.rawValue
        
        self.daysLeftLabel.text = self.presenter.daysLeftString
        
        self.setImage()
    }
}
