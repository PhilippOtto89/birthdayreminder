//
//  BirthdayListPresenter.swift
//  BirthdayReminder
//
//  Created by Philipp Otto on 18.06.17.
//  Copyright © 2017 Philipp. All rights reserved.
//

import Foundation

class BirthdayListPresenter: MVPViewControllerPresenter {
    private weak var view: BirthdayListViewProtocol?
    
    private lazy var filterService: BirthdayFilterService = BirthdayFilterService()
    
    var birthdays = [Birthday]() {
        didSet {
            self.view?.showBirthdays()
        }
    }
    
    fileprivate var unfilteredBirthdays = [Birthday]() {
        didSet {
            self.filterBirthdaysThrottled()
        }
    }
    
    fileprivate var filterName: String?
    
    required init(model: ModelProtocol) {
        super.init(model: model)
    }
    
    init(model: ModelProtocol, filterService: BirthdayFilterService) {
        super.init(model: model)
        
        self.filterService = filterService
    }
    
    override func attachView(_ view: ViewControllerProtocol) {
        guard let view = view as? BirthdayListViewProtocol else {
            assertionFailure("The given view does not implement the correct protocol for this presenter")
            return
        }
        
        self.view = view
    }
    
    override func viewWillAppear() {
        super.viewWillAppear()
        
        self.observeBirthdays()
    }
    
    override func viewWillDisappear() {
        super.viewWillDisappear()
        
        self.model.stopObservingBirthdays()
    }
    
    fileprivate func observeBirthdays() {
        self.view?.showLoading()
        
        self.model.observeBirthdays { (result) in
            switch result {
            case .error(let error):
                print(error)
                self.view?.showErrorAlert(withTitle: "Ooops".localized, message: "Loading your birthdays failed. Please try again!".localized)
            case .success(let birthdays):
                self.view?.hideLoading()
                
                self.unfilteredBirthdays = birthdays.sorted(by: { $0.daysLeft < $1.daysLeft })
            }
        }
    }
    
    func addBirthday() {
        let addBirthdayView = AddAndEditBirthdayViewController.instanceFromStoryboard()
        
        self.view?.showViewController(addBirthdayView)
    }
    
    func showDetails(forBirthdayAtIndex index: Int) {
        let birthday = self.birthdays[index]
        
        let detailPresenter = BirthdayDetailPresenter.init(model: Constants.standardModel, birthday: birthday)
        
        let detailView = BirthdayDetailViewController.instanceFromStoryboard(presenter: detailPresenter)
        
        self.view?.showViewController(detailView)
    }
    
    // Throttling the filte ris not necessary here and only done to be as near as possible to the LastFM example
    func filterBirthdays(byName name: String) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.filterBirthdaysThrottled), object: nil)
        
        self.filterName = name
        
        let delay = name.isEmpty ? 0.0 : 0.5
        
        self.perform(#selector(self.filterBirthdaysThrottled), with: nil, afterDelay: delay)
    }
    
    @objc fileprivate func filterBirthdaysThrottled() {
        self.birthdays = self.filterService.filter(self.unfilteredBirthdays, byName: self.filterName)
    }
}
