//
//  BirthdayListCellViewProtocol.swift
//  BirthdayReminder
//
//  Created by Philipp Otto on 25.06.17.
//  Copyright © 2017 Philipp. All rights reserved.
//

protocol BirthdayListCellViewProtocol: ViewProtocol {
    func showBirthday()
}
