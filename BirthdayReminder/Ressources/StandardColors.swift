//
//  StandardColors.swift
//
//  Created by Philipp Otto on 09/10/16.
//  Copyright © 2016 Philipp Otto. All rights reserved.
//

import UIKit

struct StandardColors
{
    static var primaryColor: UIColor
        {
            return #colorLiteral(red: 0.3286766112, green: 0.688675642, blue: 0.5328369737, alpha: 1)
    }
    
    static var secondaryColor: UIColor
    {
        return #colorLiteral(red: 0.9560521245, green: 0.6246490479, blue: 0.4661029577, alpha: 1)
    }
}
