//
//  Constants.swift
//
//  Created by Philipp Otto on 09/10/16.
//  Copyright © 2016 Philipp Otto. All rights reserved.
//

import UIKit

@objc class Constants: NSObject {
    static var webserviceBaseURL: String {
        return "TBD"
    }
    
    static var standardModel: ModelProtocol {
        let environment = ProcessInfo.processInfo.environment
        
        if let _ = environment["isUiTest"] {
            return MockupModel()
        }
        else {
            return FirebaseModel()
        }
    }
    
    static var firebaseStorageReferenceURL: String {
        return "gs://birthday-reminder-a7c2e.appspot.com"
    }
}
