//
//  AppDelegate.swift
//  BirthdayReminder
//
//  Created by Philipp Otto on 17.06.17.
//  Copyright © 2017 Philipp. All rights reserved.
//

import UIKit
import FirebaseCore

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FIRApp.configure()
        
        return true
    }
}

