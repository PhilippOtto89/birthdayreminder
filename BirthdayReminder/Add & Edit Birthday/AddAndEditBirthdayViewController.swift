//
//  AddAndEditBirthdayViewController.swift
//
//  Created on 22/06/17.
//  Copyright © 2017. All rights reserved.
//

import UIKit

class AddAndEditBirthdayViewController: MVPViewController<AddAndEditBirthdayPresenter> {
    
    @IBOutlet weak fileprivate var imageView: PersonImageView!
    @IBOutlet weak fileprivate var loadImageActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak fileprivate var editImageButton: UIButton!
    @IBOutlet weak fileprivate var nameTextField: UITextField!
    @IBOutlet weak fileprivate var datePicker: UIDatePicker!

    fileprivate var imagePickerHelper: ImagePickerHelper?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 11.0, *) {
            self.navigationItem.largeTitleDisplayMode = .never
        }
        
        self.setEditProfileImageButton()
    }
    
    func setEditProfileImageButton()
    {
        self.editImageButton.layer.cornerRadius = self.editImageButton.bounds.height / 2
        self.editImageButton.setBackgroundColor(color: UIColor.clear, forState: .normal)
        self.editImageButton.setBackgroundColor(color: UIColor.white.withAlpha(0.8), forState: .highlighted)
        self.editImageButton.clipsToBounds = true
    }
    
    @IBAction func editProfileImage(_ sender: Any) {
        self.imagePickerHelper = ImagePickerHelper(viewController: self, delegate: self)
        
        self.imagePickerHelper?.letUserPickImage()
    }
    
    @IBAction func doneTouchUpInside(_ sender: Any) {
        guard let name = self.nameTextField.text?.trimmingCharacters(in: [" "]), !name.isEmpty else {
            return
        }
        
        self.presenter.saveBirthday(name: name, date: self.datePicker.date)
    }
    
    // MARK: - UITextFieldDelegate -
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

// MARK: - ImagePickerHelperDelegate -
extension AddAndEditBirthdayViewController: ImagePickerHelperDelegate {
    func imagePickerHelper(_ helper: ImagePickerHelper, didFinishWithCroppedImage croppedImage: UIImage?) {
        self.imagePickerHelper = nil
        
        self.imageView.image = croppedImage ?? #imageLiteral(resourceName: "ProfileImageStandard")
        self.presenter.setImage(croppedImage)
    }
}

// MARK: - CreateBirthdayViewProtocol -
extension AddAndEditBirthdayViewController: AddAndEditBirthdayViewProtocol {
    func showBirthday(_ birthday: Birthday) {
        self.nameTextField.text = birthday.name
        
        self.datePicker.date = birthday.date
        
        if let imageUrl = birthday.imageUrl {
            self.setImage(forUrl: imageUrl)
        }
    }
    
    fileprivate func setImage(forUrl urlString: String) {
        guard let imageUrl = URL(string: urlString) else {
            self.imageView.image = #imageLiteral(resourceName: "ProfileImageStandard")
            return
        }
        
        self.loadImageActivityIndicator.startAnimating()
        
        self.imageView.af_setImage(withURL: imageUrl, placeholderImage: nil, filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .crossDissolve(0.2), runImageTransitionIfCached: false) { response in
            self.loadImageActivityIndicator.stopAnimating()
        }
    }
}
