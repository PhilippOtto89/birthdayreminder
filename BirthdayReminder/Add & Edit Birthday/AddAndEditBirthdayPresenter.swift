//
//  AddEditBirthdayPresenter.swift
//
//  Created on 22/06/17.
//  Copyright © 2017. All rights reserved.
//

import Foundation
import UIKit // For UIImage

enum AddAndEditBirthdayPresenterMode {
    case add
    case edit(birthday: Birthday)
}

protocol AddAndEditBirthdayPresenterDelegate: class {
    func addAndEditBirthdayPresenter(presenter: AddAndEditBirthdayPresenter, didSaveBirthday birthday: Birthday)
}

class AddAndEditBirthdayPresenter: MVPViewControllerPresenter {
    private weak var view: AddAndEditBirthdayViewProtocol?
    
    weak var delegate: AddAndEditBirthdayPresenterDelegate?
    
    fileprivate var imageChangeStatus: ImageChangeStatus = .unchanged
    
    // Is only set if this presenter was given a birthday to edit
    fileprivate(set) var mode: AddAndEditBirthdayPresenterMode = .add
    
    required init(model: ModelProtocol) {
        super.init(model: model)
    }
    
    init(model: ModelProtocol, mode: AddAndEditBirthdayPresenterMode) {
        self.mode = mode
        
        super.init(model: model)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch self.mode {
        case .edit(birthday: let birthday):
            self.view?.showBirthday(birthday)
        case .add: break
        }
    }
    
    override func attachView(_ view: ViewControllerProtocol) {
        guard let view = view as? AddAndEditBirthdayViewProtocol else {
            assertionFailure("The given view does not implement the correct protocol for this presenter")
            return
        }
        
        self.view = view
    }
    
    func setImage(_ image: UIImage?) {
        self.imageChangeStatus = .changed(image: image)
    }
    
    func saveBirthday(name: String, date: Date) {
        switch self.mode {
        case .add:
            switch self.imageChangeStatus {
            case .unchanged:
                self.addBirthday(forName: name, image: nil, date: date)
            case .changed(let image):
                self.addBirthday(forName: name, image: image, date: date)
            }
        case .edit(let birthday):
            self.editBirthday(withId: birthday.id, forName: name, imageUrl: birthday.imageUrl, date: date)
        }
    }
    
    fileprivate func editBirthday(withId id: String, forName name: String, imageUrl: String?, date: Date) {
        self.view?.showLoading()
        
        self.model.editBirthday(withId: id, name: name, date: date, imageChangeStatus: self.imageChangeStatus) { (result) in
            self.view?.hideLoading()
            
            switch result {
            case .error(let error):
                print(error)
                self.view?.showErrorAlert(withTitle: "Ooops".localized, message: "Adding the birthday failed. Please try again!".localized)
            case .success(let birthday):
                self.delegate?.addAndEditBirthdayPresenter(presenter: self, didSaveBirthday: birthday)
                
                self.view?.dispose()
            }
        }
    }
    
    func addBirthday(forName name: String, image: UIImage?, date: Date) {
        self.view?.showLoading()
        
        self.model.addBirthday(forName: name, image: image, date: date) { (result) in
            self.view?.hideLoading()
            
            switch result {
            case .error(let error):
                print(error)
                self.view?.showErrorAlert(withTitle: "Ooops".localized, message: "Adding the birthday failed. Please try again!".localized)
            case .success(_):
                self.view?.dispose()
            }
        }
    }
}
