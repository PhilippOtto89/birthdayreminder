//
//  CreateBirthdayViewProtocol.swift
//
//  Created on 22/06/17.
//  Copyright © 2017. All rights reserved.
//

protocol AddAndEditBirthdayViewProtocol: ViewControllerProtocol {
    func showBirthday(_ birthday: Birthday)
}
