//
//  AddAndEditBirthdayViewMockup.swift
//  BirthdayReminderTests
//
//  Created by Philipp Otto on 28.06.17.
//  Copyright © 2017 Philipp. All rights reserved.
//

import Foundation
import UIKit

class AddAndEditBirthdayViewMockup: AddAndEditBirthdayViewProtocol {
    var showBirthdayCount = 0
    var showViewControllerCount = 0
    var showLoadingCount = 0
    var hideLoadingCount = 0
    var disposeCount = 0
    var showErrorCount = 0
    
    var savedBirthday: Birthday?
    
    func showBirthday(_ birthday: Birthday) {
        self.showBirthdayCount += 1
    }
    
    func showViewController(_ viewController: UIViewController) {
        self.showViewControllerCount += 1
    }
    
    func showLoading() {
        self.showLoadingCount += 1
    }
    
    func hideLoading() {
        self.hideLoadingCount += 1
    }
    
    func dispose() {
        self.disposeCount += 1
    }
    
    func dispose(animated: Bool) {
        self.disposeCount += 1
    }
    
    func showErrorAlert(withTitle title: String, message: String) {
        self.showErrorCount += 1
    }
}

// MARK: - AddAndEditBirthdayPresenterDelegate -
extension AddAndEditBirthdayViewMockup: AddAndEditBirthdayPresenterDelegate {
    func addAndEditBirthdayPresenter(presenter: AddAndEditBirthdayPresenter, didSaveBirthday birthday: Birthday) {
        self.savedBirthday = birthday
    }
}
