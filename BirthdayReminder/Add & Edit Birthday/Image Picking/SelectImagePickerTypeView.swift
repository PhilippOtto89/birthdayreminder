//
//  SelectImagePickerTypeView.swift
//  Coloride
//
//  Created by Philipp Otto on 27/10/2016.
//  Copyright © 2016 SwissRE. All rights reserved.
//

import UIKit
import AVFoundation

protocol SelectImagePickerTypeViewDelegate: class
{
    func selectImagePickerTypeView(_ view: SelectImagePickerTypeView, didChooseSourceType sourceType: UIImagePickerControllerSourceType)
    func selectImagePickerTypeViewDidChooseToDeleteCurrentImage(_ view: SelectImagePickerTypeView)
}

class SelectImagePickerTypeView: UIView
{
    /// Height to hide the picker before animating it in.
    let neededHeight: CGFloat = 244
    /// Needed to animate the view in and out.
    var bottomConstant: NSLayoutConstraint?
    
    weak var delegate: SelectImagePickerTypeViewDelegate?
    
    private var overlayView : UIView!
    
    private weak var parentView: UIView?
    
    @IBOutlet fileprivate weak var takePhotoButton: RoundButton!
    @IBOutlet fileprivate weak var photoLibraryButton: RoundButton!
    @IBOutlet fileprivate weak var deletePhotoButton: RoundButton!
    @IBOutlet fileprivate weak var cancelButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.takePhotoButton.setTitle("Take photo".localized, for: .normal)
        self.photoLibraryButton.setTitle("Use photo from library".localized, for: .normal)
        self.deletePhotoButton.setTitle("Delete current photo".localized, for: .normal)
        self.cancelButton.setTitle("Cancel".localized, for: .normal)
        
        self.hideNoPhotoButtons()
    }
    
    func show(inView parentView: UIView)
    {
        self.hideNoPhotoButtons()
        
        self.parentView = parentView
        
        self.overlayView = UIView(frame: parentView.bounds)
        self.overlayView.backgroundColor = UIColor.black.withAlpha(0.5)
        self.overlayView.alpha = 0.0
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.hide))
        self.overlayView.addGestureRecognizer(tapGestureRecognizer)
        
        DispatchQueue.main.async {
            parentView.addSubview(self.overlayView)
            parentView.addConstraintsToAllSides(self.overlayView)
            
            parentView.addSubview(self)
            parentView.addConstraintsToSides(view: self, sides: [NSLayoutAttribute.leading, .trailing])
            
            let btmCnst = NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: parentView, attribute: .bottom, multiplier: 1, constant: self.neededHeight)
            
            parentView.addConstraint(btmCnst)
            self.bottomConstant = btmCnst
            // places the view under the bottom
            parentView.layoutIfNeeded()
            
            UIView.animate(withDuration: 0.3, animations: {
                // the height is layoutet by the xib itself
                btmCnst.constant = 0
                self.overlayView.alpha = 1.0
                // animates the view in right on the tabbar
                parentView.layoutIfNeeded()
            })
        }
    }
    
    func hide()
    {
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.bottomConstant?.constant = self.neededHeight
            self.overlayView?.alpha = 0.0
            // animate the view out at the bottom
            self.parentView?.layoutIfNeeded()
        }, completion: { (finished) -> Void in
            self.overlayView?.removeFromSuperview()
            self.overlayView = nil
            
            self.removeFromSuperview()
        })
    }
    
    /// Hides or shows some buttons depending on if the own user image is available.
    private func hideNoPhotoButtons() {
        
        let noPhotoAvailable: Bool = false
        let noPhotoHiddenButtons: [UIButton] = [self.deletePhotoButton]
        noPhotoHiddenButtons.forEach({ $0.isHidden = noPhotoAvailable})
    }
    
    // MARK: - Buttons actions
    
    @IBAction func usePhotoFromLibrary(_ sender: Any) {
        self.hide()
        self.delegate?.selectImagePickerTypeView(self, didChooseSourceType: .photoLibrary)
    }
    
    @IBAction func takePhoto(_ sender: Any) {
        self.hide()
        self.delegate?.selectImagePickerTypeView(self, didChooseSourceType: .camera)
    }
    
    @IBAction func deleteCurrentPhoto(_ sender: Any) {
        self.hide()
        self.delegate?.selectImagePickerTypeViewDidChooseToDeleteCurrentImage(self)
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.hide()
    }
    
}
