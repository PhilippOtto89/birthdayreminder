//
//  ImageCropperHelper.swift
//
//
//  Created by Philipp Otto on 17/01/2017.
//  Copyright © 2017. All rights reserved.
//

import UIKit
import RSKImageCropper

protocol ImagePickerHelperDelegate: class
{
    func imagePickerHelper(_ helper: ImagePickerHelper, didFinishWithCroppedImage croppedImage: UIImage?)
}

// Handles all the delegate and data source code for RSKImageCropper in a single helper class
class ImagePickerHelper: NSObject
{
    fileprivate weak var parentViewController: UIViewController?
    
    weak var delegate: ImagePickerHelperDelegate?
    
    var cropViewController: RSKImageCropViewController!
    
    fileprivate  let imagePicker = UIImagePickerController()
    
    fileprivate lazy var loadingView: LoadingView = { return LoadingView() }()
    
    init(viewController: UIViewController, delegate: ImagePickerHelperDelegate?)
    {
        self.parentViewController = viewController
        self.delegate = delegate
    }
    
    func letUserPickImage()
    {
        guard let parentViewController = self.parentViewController else
        {
            return
        }
        
        let selectImagePickerTypeView: SelectImagePickerTypeView = .fromNib()
        
        selectImagePickerTypeView.delegate = self
        
        let viewForShow: UIView = parentViewController.navigationController?.view ?? parentViewController.view
        
        selectImagePickerTypeView.show(inView: viewForShow)
    }
    
    fileprivate func letUserCropImage(_ image: UIImage)
    {
        self.cropViewController = RSKImageCropViewController(image: image)
        
        self.cropViewController.delegate = self
        
        self.imagePicker.present(self.cropViewController, animated: true, completion: nil)
    }
}

// MARK: - UIImagePickerControllerDelegate
extension ImagePickerHelper: UINavigationControllerDelegate
{
    
}

// MARK: - UIImagePickerControllerDelegate
extension ImagePickerHelper: UIImagePickerControllerDelegate
{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.parentViewController?.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        {
            self.letUserCropImage(image)
        }
    }
}

// MARK: - RSKImageCropViewControllerDelegate
extension ImagePickerHelper: RSKImageCropViewControllerDelegate
{
    func imageCropViewControllerDidCancelCrop(_ controller: RSKImageCropViewController) {
        self.imagePicker.dismiss(animated: true)
    }
    
    func imageCropViewController(_ controller: RSKImageCropViewController, willCropImage originalImage: UIImage) {
        self.loadingView.show(inView: self.cropViewController.view, tabBarView: nil, completion: nil)
    }
    
    func imageCropViewController(_ controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect) {
        self.finishCroppingWithImage(croppedImage)
    }
    
    func imageCropViewController(_ controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect, rotationAngle: CGFloat) {
        self.finishCroppingWithImage(croppedImage)
    }
    
    private func finishCroppingWithImage(_ image: UIImage)
    {
        self.loadingView.hide(animated: true, completion: nil)
        
        self.imagePicker.dismiss(animated: true) {
            self.parentViewController?.dismiss(animated: true, completion: nil)
        }
        
        delegate?.imagePickerHelper(self, didFinishWithCroppedImage: image)
    }
}

extension ImagePickerHelper: SelectImagePickerTypeViewDelegate
{
    func selectImagePickerTypeView(_ view: SelectImagePickerTypeView, didChooseSourceType sourceType: UIImagePickerControllerSourceType) {
        guard let parentViewController = self.parentViewController else
        {
            return
        }
        
        if UIImagePickerController.isSourceTypeAvailable(sourceType){
            imagePicker.delegate = self
            imagePicker.sourceType = sourceType;
            imagePicker.allowsEditing = false
            
            self.loadingView.show(inView: parentViewController.view, tabBarView: nil, completion: nil)
            
            parentViewController.present(imagePicker, animated: true, completion: {
                self.loadingView.hide(animated: true, completion: nil)
            })
        }
    }
    
    func selectImagePickerTypeViewDidChooseToDeleteCurrentImage(_ view: SelectImagePickerTypeView) {
        self.delegate?.imagePickerHelper(self, didFinishWithCroppedImage: nil)
    }
}
