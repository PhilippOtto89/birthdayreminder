//
//  BirthdayFilterService.swift
//  BirthdayReminder
//
//  Created by Philipp Otto on 28.06.17.
//  Copyright © 2017 Philipp. All rights reserved.
//

import Foundation

// This class is only for demo purposes and would be left out in a real world app. It is just implemented to show how presenters could use additional services
class BirthdayFilterService {
    
    func filter(_ birthdays: [Birthday], byName name: String?) -> [Birthday] {
        guard let name = name?.lowercased(), !name.isEmpty else {
            return birthdays
        }
        
        return birthdays.filter( { $0.name.lowercased().contains(name) } )
    }
    
}
