//
//  BirthdayDetailPresenter.swift
//  BirthdayReminder
//
//  Created by Philipp Otto on 18.06.17.
//  Copyright © 2017 Philipp. All rights reserved.
//

import Foundation

class BirthdayDetailPresenter: MVPViewControllerPresenter {
    fileprivate weak var view: BirthdayDetailViewProtocol?
    
    fileprivate(set) var birthday: Birthday
    
    required init(model: ModelProtocol) {
        self.birthday = Birthday()
        
        super.init(model: model)
    }
    
    init(model: ModelProtocol, birthday: Birthday) {
        self.birthday = birthday
        
        super.init(model: model)
    }
    
    override func attachView(_ view: ViewControllerProtocol) {
        guard let view = view as? BirthdayDetailViewProtocol else {
            assertionFailure("The given view does not implement the correct protocol for this presenter")
            return
        }
        
        self.view = view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view?.showBirthday()
    }
    
    var daysLeftString: String {
        if self.birthday.daysLeft <= 0 {
            return "Birthday is today".localized
        }
        
        return "Next birthday is in \(self.birthday.daysLeft) days"
    }
    
    func deleteBirthday() {
        self.view?.showLoading()
        
        self.model.deleteBirthday(withId: self.birthday.id) { (response) in
            switch response {
            case .error(let error):
                print(error)
                self.view?.showErrorAlert(withTitle: "Ooops".localized, message: "Deleting the birthday failed. Please try again.".localized)
            case .success():
                self.view?.hideLoading()
                self.view?.dispose()
            }
        }
    }
    
    func editBirthday() {
        let editPresenter = AddAndEditBirthdayPresenter(model: Constants.standardModel, mode: .edit(birthday: self.birthday))
        
        editPresenter.delegate = self
        
        let addView = AddAndEditBirthdayViewController.instanceFromStoryboard(presenter: editPresenter)
        
        self.view?.showViewController(addView)
    }
}

// MARK: - AddAndEditBirthdayPresenterDelegate -
extension BirthdayDetailPresenter: AddAndEditBirthdayPresenterDelegate {
    func addAndEditBirthdayPresenter(presenter: AddAndEditBirthdayPresenter, didSaveBirthday birthday: Birthday) {
        self.birthday = birthday
        
        self.view?.showBirthday()
    }
}
