//
//  BirthdayDetailViewProtocol.swift
//
//  Created by Philipp Otto on 09/10/16.
//  Copyright © 2016 Philipp Otto. All rights reserved.
//

protocol BirthdayDetailViewProtocol: ViewControllerProtocol {
    func showBirthday()
}
