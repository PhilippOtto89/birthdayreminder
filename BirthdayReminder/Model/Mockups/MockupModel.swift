//
//  MockupModel.swift
//  BirthdayReminderTests
//
//  Created by Philipp Otto on 28.06.17.
//  Copyright © 2017 Philipp. All rights reserved.
//

import Foundation
import UIKit

class MockupModel: ModelProtocol {
    
    static let presetBirthdays = [Birthday(id: "ID1", name: "Philipp", date: Date.from(year: 1989, month: 5, day: 30), imageUrl: nil),
                                  Birthday(id: "ID2", name: "Peter", date: Date.from(year: 1980, month: 2, day: 12), imageUrl: nil),
                                  Birthday(id: "ID3", name: "Susi", date: Date.from(year: 1992, month: 8, day: 30), imageUrl: nil),
                                  Birthday(id: "ID4", name: "Max", date: Date.from(year: 1985, month: 12, day: 16), imageUrl: nil)]
    
    static var birthdays = presetBirthdays
    
    static func reset() {
        self.birthdays = presetBirthdays
    }
    
    func addBirthday(forName name: String, image: UIImage?, date: Date, completion: @escaping (ModelResult<Birthday>) -> Void) {
        let birthday = Birthday(id: UUID().uuidString, name: name, date: date, imageUrl: nil)
        
        MockupModel.birthdays.append(birthday)
        
        completion(.success(birthday))
    }
    
    func editBirthday(withId id: String, name: String, date: Date, imageChangeStatus: ImageChangeStatus, completion: @escaping (ModelResult<Birthday>) -> Void) {
        
        self.deleteBirthday(withId: id) { (response) in
            switch response {
            case .error(let error):
                completion(.error(error))
            case .success():
                let editedBirthday = Birthday(id: id, name: name, date: date, imageUrl: nil)
                
                MockupModel.birthdays.append(editedBirthday)
                
                completion(.success(editedBirthday))
            }
        }
    }
    
    func deleteBirthday(withId id: String, completion: @escaping (ModelResponse) -> Void) {
        for (index, birthday) in MockupModel.birthdays.enumerated() {
            if birthday.id == id {
                MockupModel.birthdays.remove(at: index)
                completion(.success())
                return
            }
        }
        
        completion(.error(NSError(domain: "Birthday not found", code: 404, userInfo: nil)))
    }
    
    func observeBirthdays(completion: @escaping (ModelResultList<Birthday>) -> Void) {
        completion(.success(MockupModel.birthdays))
    }
    
    func stopObservingBirthdays() {}
}
