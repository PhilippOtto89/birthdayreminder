//
//  Birthday+Firebase.swift
//  BirthdayReminder
//
//  Created by Philipp Otto on 25.06.17.
//  Copyright © 2017 Philipp. All rights reserved.
//

import Foundation

extension Birthday {
    func toFirebaseDictionary() -> Dictionary<String,Any> {
        var dictionary = Dictionary<String,Any>()
        
        dictionary["name"] = self.name
        dictionary["date"] = self.date.firebaseStringFormat
        
        if let imageUrl = self.imageUrl {
            dictionary["imageUrl"] = imageUrl
        }
        
        return dictionary
    }
    
    static func fromFirebaseDictionary(dictionary: Dictionary<String,Any>, forKey key: String) -> Birthday {
        guard let name = dictionary["name"] as? String, let dateString = dictionary["date"] as? String else {
            assertionFailure("Error: Invalid Birthday dictionary")
            return Birthday()
        }
        
        let imageUrl = dictionary["imageUrl"] as? String
        
        let date = Date.fromFirebaseFormat(dateString: dateString)
        
        return Birthday(id: key, name: name, date: date, imageUrl: imageUrl)
    }
}
