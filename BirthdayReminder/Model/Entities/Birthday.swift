//
//  Birthday.swift
//  BirthdayReminder
//
//  Created by Philipp Otto on 19.06.17.
//  Copyright © 2017 Philipp. All rights reserved.
//

import Foundation

struct Birthday {
    fileprivate(set) var id: String = UUID().uuidString
    
    var imageUrl: String?
    
    var name: String = "No Data"
    
    var date: Date = Date() {
        didSet {
            self.dateChanged()
        }
    }
    
    fileprivate(set) var daysLeft: Int = 0
    
    fileprivate(set) var starSign: StarSign = .unknown
    
    fileprivate(set) var age: Int = 0
    
    init() {}
    
    init(id: String, name: String, date: Date, imageUrl: String?) {
        self.id = id
        self.name = name
        self.date = date
        self.imageUrl = imageUrl
        
        self.dateChanged()
    }
    
    fileprivate mutating func dateChanged() {
        self.daysLeft = self.getDaysLeft(untilBirthdayDate: date)
        self.starSign = StarSign.forDate(date)
        self.age = Date().getYearDifference(toDate: self.date)
    }
    
    func getDaysLeft(untilBirthdayDate date: Date) -> Int {
        let birthdayDateComponents = date.dateComponents
        
        guard let currentYear = Date().dateComponents.year, let birthdayDay = birthdayDateComponents.day, let birthdayMonth = birthdayDateComponents.month else {
            return -1
        }
        
        var nextBirthday = Date.from(year: currentYear, month: birthdayMonth, day: birthdayDay)
        
        if nextBirthday < Date().startOfDay {
            nextBirthday = Date.from(year: currentYear + 1, month: birthdayMonth, day: birthdayDay)
        }
        
        return nextBirthday.getDayDifference(toDate: Date().startOfDay)
    }
}
