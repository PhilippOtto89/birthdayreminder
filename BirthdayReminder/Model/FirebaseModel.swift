//
//  FirebaseModel.swift
//  BirthdayReminder
//
//  Created by Philipp Otto on 18.06.17.
//  Copyright © 2017 Philipp. All rights reserved.
//

import Foundation
import UIKit
import FirebaseDatabase
import FirebaseStorage

class FirebaseModel: ModelProtocol {
    private var ref: FIRDatabaseReference = FIRDatabase.database().reference()
    
    private var birthdaysObserverHandle: FIRDatabaseHandle?
    
    var userId: String {
        return UIDevice.current.identifierForVendor?.uuidString ?? "Unknown user"
    }
    
    var birthdaysRef: FIRDatabaseReference {
        return self.ref.child("\(self.userId)").child("birthdays")
    }
    
    var imagesRef: FIRStorageReference {
        let storageRef = FIRStorage.storage().reference(forURL: Constants.firebaseStorageReferenceURL)
        
        return storageRef.child("\(self.userId)").child("images")
    }
    
    func addBirthday(forName name: String, image: UIImage?, date: Date, completion: @escaping (ModelResult<Birthday>) -> Void) {
        guard let image = image else {
            self.addBirthdayToFirebase(forName: name, imageUrl: nil, date: date, completion: completion)
            return
        }
        
        self.upload(image: image, name: name) { (result) in
            switch result {
            case .error(let error): completion(.error(error))
            case .success(let imageUrl):
                self.addBirthdayToFirebase(forName: name, imageUrl: imageUrl, date: date, completion: completion)
            }
        }
    }
    
    fileprivate func addBirthdayToFirebase(forName name: String, imageUrl: String?, date: Date, completion: @escaping (ModelResult<Birthday>) -> Void) {
        let newBirthdayRef = self.birthdaysRef.childByAutoId()
        
        let birthday = Birthday(id: newBirthdayRef.key, name: name, date: date, imageUrl: imageUrl)
        
        newBirthdayRef.setValue(birthday.toFirebaseDictionary()) { (error, reference) in
            if let error = error {
                completion(.error(error))
                return
            }
            
            DispatchQueue.main.async {
                completion(.success(birthday))
            }
        }
    }
    
    func editBirthday(withId id: String, name: String, date: Date, imageChangeStatus: ImageChangeStatus, completion: @escaping (ModelResult<Birthday>) -> Void) {
        self.getBirthday(withId: id) { (result) in
            switch result {
            case .error(let error):
                completion(.error(error))
            case .success(var birthday):
                let birthdayRef = self.birthdaysRef.child(id)
                
                birthday.name = name
                birthday.date = date
                
                birthdayRef.setValue(birthday.toFirebaseDictionary()) { (error, reference) in
                    if let error = error {
                        completion(.error(error))
                        return
                    }
                    
                    completion(.success(birthday))
                }
            }
        }
    }
    
    fileprivate func getBirthday(withId id: String, completion: @escaping (ModelResult<Birthday>) -> Void) {
        let birthdayRef = self.birthdaysRef.child(id)
        
        birthdayRef.observeSingleEvent(of: .value, with: { (snapshot) -> Void in
            guard let dictionary = snapshot.value as? Dictionary<String, Any> else {
                completion(.error(NSError(domain: "Birthday not found", code: 404, userInfo: nil)))
                return
            }
            
            let birthday = Birthday.fromFirebaseDictionary(dictionary: dictionary, forKey: id)
            
            completion(.success(birthday))
        })
    }
    
    func deleteBirthday(withId id: String, completion: @escaping (ModelResponse) -> Void) {
        let birthdayRef = self.birthdaysRef.child(id)
        
        birthdayRef.removeValue { (error, reference) in
            if let error = error {
                completion(.error(error))
            }
            
            completion(.success())
        }
    }
    
    func observeBirthdays(completion: @escaping (ModelResultList<Birthday>) -> Void) {
        self.birthdaysObserverHandle = self.birthdaysRef.observe(.value, with: { (snapshot) in
            
            guard let dictionary = snapshot.value as? Dictionary<String,Dictionary<String,Any>> else {
                return
            }
            
            var birthdays = [Birthday]()
            
            for (key, value) in dictionary {
                let birthday = Birthday.fromFirebaseDictionary(dictionary: value, forKey: key)
                
                birthdays.append(birthday)
            }
            
            completion(.success(birthdays))
        }) { (error) in
            print("Cancelled")
        }
    }
    
    func stopObservingBirthdays() {
        guard let birthdaysObserverHandle = self.birthdaysObserverHandle else {
            return
        }
        
        self.birthdaysRef.removeObserver(withHandle: birthdaysObserverHandle)
    }
    
    // MARK: - Image Handling -
    
    func upload(image: UIImage, name: String, completion: @escaping (ModelResult<String>) -> Void)
    {
        let resizedImage = image.resize(toSize: CGSize(width: 400, height: 400))
        
        let imageData = UIImageJPEGRepresentation(resizedImage, 0.5)!
        
        let imageRef = self.imagesRef.child("\(name).jpg")
        
        let metadata = FIRStorageMetadata()
        metadata.contentType = "image/jpeg"
        metadata.cacheControl = "public,max-age=31536000"
        
        let uploadTask = imageRef.put(imageData, metadata: metadata) { (metadata, error) in
            if let error = error {
                completion(.error(error as NSError))
            }
            
            guard let metadata = metadata, let downloadURL = metadata.downloadURL()?.absoluteString else {
                completion(.error(NSError(domain: "No metadata for uploaded image", code: 400, userInfo: nil)))
                return
            }
            
            completion(.success(downloadURL))
        }
        
        uploadTask.observe(.progress) { snapshot in
            print("Progress: \(String(describing: snapshot.progress))")
        }
    }
}

