//
//  StarSign.swift
//  BirthdayReminder
//
//  Created by Philipp Otto on 25.06.17.
//  Copyright © 2017 Philipp. All rights reserved.
//

import Foundation

enum StarSign: String {
    case capricorn = "Capricorn"
    case aquarius = "Aquarius"
    case pisces = "Pisces"
    case aries = "Aries"
    case taurus = "Taurus"
    case gemini = "Gemini"
    case cancer = "Cancer"
    case leo = "Leo"
    case virgo = "Virgo"
    case libra = "Libra"
    case scorpio = "Scorpio"
    case sagittarius = "Sagittarius"
    case unknown = "Unknown"
    
    static func forDate(_ date: Date) -> StarSign {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.day, .month], from: date)
        
        guard let day = components.day, let month = components.month else {
            return .unknown
        }
        
        if (month == 12 && day >= 22 && day <= 31) || (month ==  1 && day >= 1 && day <= 19) {
            return .capricorn
        }
        else if (month ==  1 && day >= 20 && day <= 31) || (month ==  2 && day >= 1 && day <= 17) {
            return .aquarius
        }
        else if (month ==  2 && day >= 18 && day <= 29) || (month ==  3 && day >= 1 && day <= 19) {
            return .pisces
        }
        else if (month ==  3 && day >= 20 && day <= 31) || (month ==  4 && day >= 1 && day <= 19) {
             return .aries
        }
        else if (month ==  4 && day >= 20 && day <= 30) || (month ==  5 && day >= 1 && day <= 20) {
             return .taurus
        }
        else if (month ==  5 && day >= 21 && day <= 31) || (month ==  6 && day >= 1 && day <= 20) {
            return .gemini
        }
        else if (month ==  6 && day >= 21 && day <= 30) || (month ==  7 && day >= 1 && day <= 22) {
            return .cancer
        }
        else if (month ==  7 && day >= 23 && day <= 31) || (month ==  8 && day >= 1 && day <= 22) {
            return .leo
        }
        else if (month ==  8 && day >= 23 && day <= 31) || (month ==  9 && day >= 1 && day <= 22) {
            return .virgo
        }
        else if (month ==  9 && day >= 23 && day <= 30) || (month == 10 && day >= 1 && day <= 22) {
            return .libra
        }
        else if (month == 10 && day >= 23 && day <= 31) || (month == 11 && day >= 1 && day <= 21) {
            return .scorpio
        }
        else if (month == 11 && day >= 22 && day <= 30) || (month == 12 && day >= 1 && day <= 21) {
            return .sagittarius
        }
        else  {
            return .unknown
        }
    }
}
