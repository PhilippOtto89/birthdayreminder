//
//  PersonImageView.swift
//  BirthdayReminder
//
//  Created by Philipp Otto on 27.06.17.
//  Copyright © 2017 Philipp. All rights reserved.
//

import UIKit

class PersonImageView: UIImageView {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.borderWidth = 1.0
        self.clipsToBounds = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.layer.cornerRadius = self.bounds.height / 2
    }
}
