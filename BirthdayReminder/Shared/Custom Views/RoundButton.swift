//
//  RoundButton.swift
//  Coloride
//
//  Created by Philipp Otto on 15/08/16.
//  Copyright © 2016 SwissRE. All rights reserved.
//

import UIKit

@IBDesignable class RoundButton: UIButton
{
    @IBInspectable var cornerRadiusPercentage: CGFloat = 0.0
        {
        didSet
        {
            let cornerRadius = bounds.width / (100 / self.cornerRadiusPercentage)
            
            self.layer.cornerRadius = cornerRadius
            
            self.clipsToBounds = true
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0.0 {
        didSet{
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet{
            self.layer.borderColor = borderColor.cgColor
        }
    }
}
