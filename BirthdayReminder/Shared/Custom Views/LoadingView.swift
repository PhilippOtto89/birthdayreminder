//
//  LoadingView.swift
//
//  Created by Philipp Otto on 09/10/16.
//  Copyright © 2016 Philipp Otto. All rights reserved.
//

import UIKit

class LoadingView : UIView
{
    private weak var parentView : UIView?
    private weak var parentTabBarView : UIView?
    private var activitySpinner : UIActivityIndicatorView?
    private var spinnerBackgroundView : UIView?
    
    private var showCount : Int // Counts the difference between show() and hide() calls
    
    required init?(coder aDecoder: NSCoder) { // Needed for UIView subclass
        showCount = 0
        parentView = nil
        
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) { // Needed for initialization
        self.showCount = 0
        
        self.parentView = nil;
        
        super.init(frame: frame)
        
        //self.backgroundColor = UIColor.black.withAlpha(alpha: 0.3)
        
        self.spinnerBackgroundView = UIView(frame:CGRect(x: 0, y: 0, width: 80, height: 80))
        self.spinnerBackgroundView!.backgroundColor = StandardColors.primaryColor
        self.spinnerBackgroundView!.layer.cornerRadius = self.spinnerBackgroundView!.bounds.width / 10
        
        self.spinnerBackgroundView!.layer.borderColor = UIColor.black.cgColor
        self.spinnerBackgroundView!.layer.borderWidth = 1.0
        
        addSubview(self.spinnerBackgroundView!)
        
        self.activitySpinner = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        self.activitySpinner?.color = UIColor.white
        
        addSubview(activitySpinner!)
        activitySpinner!.startAnimating()
    }
    
    override func layoutSubviews() {
        
        guard let parentViewBounds = self.parentView?.bounds else {
            return
        }
        
        self.frame = parentViewBounds;
        
        let centerX : CGFloat = CGFloat(parentViewBounds.width) / 2
        let centerY : CGFloat = CGFloat(parentViewBounds.height) / 2
        
        self.activitySpinner?.center = CGPoint(x: centerX, y: centerY)
        
        self.spinnerBackgroundView?.center = CGPoint(x: centerX, y: centerY)
    }
    
    func show(inView view: UIView, animated: Bool = true, completion: (() -> Void)? = nil)
    {
        self.show(inView: view, tabBarView: nil, animated: animated, completion: completion)
    }
    
    // A tab bar view can be passed so that the whole screen gets disabled as long as the loading view is shown
    func show(inView view: UIView, tabBarView: UIView?, animated: Bool = true, completion: (() -> Void)? = nil)
    {
        showCount += 1
        
        if(parentView != nil)
        {
            return
        }
        
        parentView = view
        parentTabBarView = tabBarView
        
        DispatchQueue.main.async {
            self.parentTabBarView?.isUserInteractionEnabled = false
            self.parentView?.isUserInteractionEnabled = false
            
            view.addSubview(self)
            view.bringSubview(toFront: self)
            
            self.layoutSubviews()
            
            let animDuration = animated ? 0.5 : 0.0
            
            UIView.animate(withDuration: animDuration, animations: { () -> Void in
                self.alpha = 1.0
            }, completion: { (completed) -> Void in
                completion?()
            })
        }

    }
    
    func hide(animated: Bool = true, force: Bool = false, completion: (() -> Void)? = nil)
    {
        self.showCount = force ? 0 : self.showCount - 1

        if showCount > 0
        {
            return
        }
        
        let animDuration = animated ? 0.5 : 0.0
        
        DispatchQueue.main.async {
            UIView.animate(withDuration: animDuration, animations: { () -> Void in
                self.alpha = 0
            }, completion: { (completed) -> Void in
                self.removeFromSuperview()
                
                self.parentTabBarView?.isUserInteractionEnabled = true
                self.parentView?.isUserInteractionEnabled = true
                
                self.parentView = nil
                self.parentTabBarView = nil
                
                completion?()
            })
        }
    }
}


