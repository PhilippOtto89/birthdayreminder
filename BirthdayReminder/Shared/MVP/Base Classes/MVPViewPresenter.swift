//
//  ViewPresenter.swift
//  BirthdayReminder
//
//  Created by Philipp Otto on 18.06.17.
//  Copyright © 2017 Philipp. All rights reserved.
//

import Foundation

class MVPViewPresenter: NSObject {
    fileprivate(set) var model: ModelProtocol
    
    required init(model: ModelProtocol) {
        self.model = model
    }
    
    func attachView(_ view: ViewProtocol) {
        assertionFailure("Implement this in the respective presenter subclass")
    }
    
    // MARK: View lifecycle
    
    func initialize() {}
    
    func viewDidAwakeFromNib() {}
}
