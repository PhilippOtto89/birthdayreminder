//
//  MVPView.swift
//  BirthdayReminder
//
//  Created by Philipp Otto on 25.06.17.
//  Copyright © 2017 Philipp. All rights reserved.
//

import UIKit

class MVPView<PresenterType: MVPViewPresenter>: UIView {
    fileprivate(set) lazy var presenter: PresenterType = PresenterType(model: Constants.standardModel)
    
    // MARK: - View lifecycle -
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.commonInit()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        presenter.awakeFromNib()
    }
    
    func commonInit() {
        self.presenter.initialize()
        self.presenter.attachView(self)
    }
    
    static func fromNib(owner: AnyObject? = nil, presenter: PresenterType) -> Self {
        
        let view = self.fromNib(owner: owner)
        
        view.presenter = presenter
        
        return view
    }
}

// MARK: - ViewProtocol -
extension MVPView: ViewProtocol {
}

