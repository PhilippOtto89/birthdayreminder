//
//  ViewControllerPresenter.swift
//  BirthdayReminder
//
//  Created by Philipp Otto on 18.06.17.
//  Copyright © 2017 Philipp. All rights reserved.
//

import Foundation

class MVPViewControllerPresenter: NSObject {
    fileprivate(set) var model: ModelProtocol
    
    required init(model: ModelProtocol) {
        self.model = model
    }
    
    func attachView(_ view: ViewControllerProtocol) {
        assertionFailure("Implement this in the respective presenter subclass")
    }
    
    // MARK: View lifecycle
    
    func viewDidLoad() {}
    func viewWillAppear() {}
    func viewDidAppear() {}
    func viewWillDisappear() {}
    func viewDidDisappear() {}
    func changeOrientation() {}
}
