//
//  MVPTableViewCell.swift
//  BirthdayReminder
//
//  Created by Philipp Otto on 25.06.17.
//  Copyright © 2017 Philipp. All rights reserved.
//

import UIKit

class MVPTableViewCell<PresenterType: MVPTableViewCellPresenter>: UITableViewCell {
    fileprivate(set) lazy var presenter: PresenterType = PresenterType(model: Constants.standardModel)
    
    // MARK: - View lifecycle -
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        presenter.awakeFromNib()
    }
    
    func commonInit() {
        self.presenter.initialize()
        self.presenter.attachView(self)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.presenter.prepareForReuse()
    }
}

// MARK: - ViewProtocol -
extension MVPTableViewCell: ViewProtocol {
}

