//
//  GenericBaseViewController.swift
//  BirthdayReminder
//
//  Created by Philipp Otto on 18.06.17.
//  Copyright © 2017 Philipp. All rights reserved.
//

import UIKit

class MVPViewController<PresenterType: MVPViewControllerPresenter>: UIViewController {
    fileprivate(set) lazy var presenter: PresenterType = PresenterType(model: Constants.standardModel)
    
    fileprivate(set) lazy var loadingView: LoadingView = LoadingView()
    
    fileprivate let prefersLargeNavigationBarTitles: Bool = true
    
    class func instanceFromStoryboard(storyboardName: String = "Main", storyboardBundle: Bundle? = nil, viewControllerIdentifier: String? = nil, presenter: PresenterType) -> Self {
        
        let viewController = self.instanceFromStoryboard(storyboardName: storyboardName, storyboardBundle: storyboardBundle, viewControllerIdentifier: viewControllerIdentifier)
        
        viewController.presenter = presenter
        
        return viewController
    }
    
    fileprivate func setNavigationBar() {
        if #available(iOS 11.0, *) {
            self.navigationController?.navigationBar.prefersLargeTitles = self.prefersLargeNavigationBarTitles
        }
    }
    
    // MARK: - Loading -
    
    // Declare here and not in extension because Swift can not override extension methods yet
    func showLoading() {
        self.loadingView.show(inView: self.view, animated: true, completion: nil)
    }
    
    func hideLoading() {
        self.loadingView.hide(animated: true, force: false, completion: nil)
    }
    
    // MARK: - View lifecycle -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.presenter.attachView(self)
        self.presenter.viewDidLoad()
        
        self.setNavigationBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.presenter.viewWillAppear()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.presenter.viewDidAppear()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.presenter.viewWillDisappear()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.presenter.viewDidDisappear()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        self.presenter.changeOrientation()
    }
}

// MARK: - ViewControllerProtocol -
extension MVPViewController: ViewControllerProtocol {
    func showViewController(_ viewController: UIViewController) {
        if let navigationController = self.navigationController {
            navigationController.pushViewController(viewController, animated: true)
        }
        else {
            self.present(viewController, animated: true, completion: nil)
        }
    }
    
    func dispose() {
        self.dispose(animated: true)
    }
    
    func dispose(animated: Bool) {
        if let navigationController = self.navigationController {
            navigationController.popViewController(animated: animated)
        }
        else {
            self.dismiss(animated: animated, completion: nil)
        }
    }
    
    func showErrorAlert(withTitle title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK".localized, style: .default, handler: nil)
        
        alertController.addAction(okAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
}
