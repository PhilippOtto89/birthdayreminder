//
//  IModel.swift
//  BirthdayReminder
//
//  Created by Philipp Otto on 17.06.17.
//  Copyright © 2017 Philipp. All rights reserved.
//

import Foundation
import UIKit

enum ModelResponse
{
    case success()
    case error(Error)
}

enum ModelResultList<T>
{
    case success([T])
    case error(Error)
}

enum ModelResult<T>
{
    case success(T)
    case error(Error)
}

enum ImageChangeStatus {
    case unchanged
    case changed(image: UIImage?)
}

protocol ModelProtocol {
    
    func addBirthday(forName name: String, image: UIImage?, date: Date, completion: @escaping (ModelResult<Birthday>) -> Void)
    
    func editBirthday(withId id: String, name: String, date: Date, imageChangeStatus: ImageChangeStatus, completion: @escaping (ModelResult<Birthday>) -> Void)
    
    func deleteBirthday(withId id: String, completion: @escaping (ModelResponse) -> Void)
    
    func observeBirthdays(completion: @escaping (ModelResultList<Birthday>) -> Void)
    
    func stopObservingBirthdays()
}
