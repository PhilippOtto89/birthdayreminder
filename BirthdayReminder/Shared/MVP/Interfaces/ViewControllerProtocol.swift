//
//  ViewProtocol.swift
//
//  Created by Philipp Otto on 09/10/16.
//  Copyright © 2016 Philipp Otto. All rights reserved.
//

import UIKit

protocol ViewControllerProtocol: class {
    func showViewController(_ viewController: UIViewController)
    
    func showLoading()
    
    func hideLoading()
    
    func dispose()
    
    func dispose(animated: Bool)
    
    func showErrorAlert(withTitle title: String, message: String)
}
