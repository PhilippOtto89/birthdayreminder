//
//  UIColor+WithAlpha.swift
//
//
//  Created by Philipp Otto on 14/01/2017.
//  Copyright © 2017. All rights reserved.
//

import UIKit

extension UIColor
{
    func withAlpha(_ alpha: CGFloat) -> UIColor
    {
        var fRed : CGFloat = 0
        var fGreen : CGFloat = 0
        var fBlue : CGFloat = 0
        
        if self.getRed(&fRed, green: &fGreen, blue: &fBlue, alpha: nil) {
            return UIColor(red: fRed, green: fGreen, blue: fBlue, alpha: alpha)
        } else {
            // Could not extract RGBA components:
            return UIColor.red
        }
    }
}

