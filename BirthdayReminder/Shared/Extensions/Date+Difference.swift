//
//  Date+Difference.swift
//  BirthdayReminder
//
//  Created by Philipp Otto on 25.06.17.
//  Copyright © 2017 Philipp. All rights reserved.
//

import Foundation

extension Date {
    func getDayDifference(toDate date: Date) -> Int {
        let calendar = Calendar.current
        
        let components: DateComponents = calendar.dateComponents([.day], from: date, to: self)
        
        return components.day ?? -1
    }
    
    func getYearDifference(toDate date: Date) -> Int {
        let calendar = Calendar.current
        
        let components: DateComponents = calendar.dateComponents([.year], from: date, to: self)
        
        return components.year ?? -1
    }
    
    var dateComponents: DateComponents {
        let calendar = Calendar.current
        
        return calendar.dateComponents([.year, .month, .day], from: self)
    }
    
    var startOfDay : Date {
        let calendar = Calendar.current
        let unitFlags = Set<Calendar.Component>([.year, .month, .day])
        let components = calendar.dateComponents(unitFlags, from: self)
        return calendar.date(from: components)!
    }
}
