//
//  String+Localization.swift
//
//  Created by Philipp Otto on 04/05/16.
//  Copyright © 2016 Philipp Otto. All rights reserved.
//

import UIKit

public extension String
{
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
}
