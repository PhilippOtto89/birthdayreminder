//
//  UIImage+Resize.swift
//  BirthdayReminder
//
//  Created by Philipp Otto on 27.06.17.
//  Copyright © 2017 Philipp. All rights reserved.
//

import Foundation

import UIKit

extension UIImage {
    func resize(toSize newSize: CGSize) -> UIImage {
        
        UIGraphicsBeginImageContext(CGSize(width: newSize.width, height: newSize.height))
        self.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}
