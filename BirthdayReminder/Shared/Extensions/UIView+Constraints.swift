//
//  UIView+Constraints.swift
//
//
//  Created by Fabio Bente on 02.04.17.
//  Copyright © 2017. All rights reserved.
//

import UIKit

extension UIView {
    
    func addConstraintsToAllSides(_ view: UIView) {
        self.addConstraintsToSides(view: view, sides: [NSLayoutAttribute.leading, .trailing, .top, .bottom])
    }
    
    /// - Parameter sides: Pass sides like [NSLayoutAttribute.leading, .trailing, .top, .bottom]
    func addConstraintsToSides(view: UIView, sides: [NSLayoutAttribute]) {
        view.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = sides.map({ NSLayoutConstraint(item: view, attribute: $0, relatedBy: .equal, toItem: self, attribute: $0, multiplier: 1, constant: 0) })
        self.addConstraints(constraints)
    }
    
    func addCenterConstraints(forView view: UIView) {
        let centerXConstraint = NSLayoutConstraint(item: view, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0)
        
        let centerYConstraint = NSLayoutConstraint(item: view, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
        
        self.addConstraints([centerXConstraint, centerYConstraint])
    }
    
    func addSizeConstraints(forView view: UIView, width: CGFloat, height: CGFloat) {
        let widthConstraint = NSLayoutConstraint(item: view, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: width)
        
        let heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: height)
        
        self.addConstraints([widthConstraint, heightConstraint])
    }
}

