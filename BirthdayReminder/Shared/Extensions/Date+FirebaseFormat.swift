//
//  Date+FirebaseFormat.swift
//  BirthdayReminder
//
//  Created by Philipp Otto on 25.06.17.
//  Copyright © 2017 Philipp. All rights reserved.
//

import Foundation

extension Date {
    var firebaseStringFormat: String {
        return self.stringWithFormat(format: "yyyy-MM-dd")
    }
    
    var firebaseSortStringFormat: String {
        return self.stringWithFormat(format: "MM-dd")
    }
    
    static func fromFirebaseFormat(dateString: String) -> Date {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        return dateFormatter.date(from: dateString) ?? Date()
    }
}
