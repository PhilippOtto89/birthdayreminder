//
//  NSObject+ClassName.swift
//  BirthdayReminder
//
//  Created by Philipp Otto on 18.06.17.
//  Copyright © 2017 Philipp. All rights reserved.
//

import Foundation

extension NSObject {
    var className: String {
        return String(describing: type(of: self))
    }
    
    class var className: String {
        return String(describing: self)
    }
}
