//
//  Date+Format.swift
//  BirthdayReminder
//
//  Created by Philipp Otto on 25.06.17.
//  Copyright © 2017 Philipp. All rights reserved.
//

import Foundation

extension Date {
    func stringWithFormat(format: String) -> String {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = format
        
        return dateFormatter.string(from: self)
    }
    
    var dayMonthLongFormat: String {
        return self.stringWithFormat(format: "d. MMMM")
    }
}
