//
//  UIViewController+FromStoryboard.swift
//
//  Created by Philipp Otto on 09/08/16.
//  Copyright © 2016 Philipp Otto. All rights reserved.
//

import UIKit

extension UIViewController {
    
    // MARK: - Init
    
    /// - Parameters:
    ///     - storyboardName: Per default the "Main" storyboard is used.
    ///     - storyboardBundle: Per default the Bundle of the calling ViewController class is used.
    ///     - viewControllerIdentifier: Per default the name of the calling ViewController class is used.
    class func instanceFromStoryboard(storyboardName: String = "Main", storyboardBundle: Bundle? = nil, viewControllerIdentifier: String? = nil) -> Self {
        
        let identifier = viewControllerIdentifier ?? self.className
        let bundle = storyboardBundle ?? Bundle(for: self)
        
        return self.instanceFromStoryboardHelper(storyboardName: storyboardName, storyboardBundle: bundle, viewControllerIdentifier: identifier)
    }
    
    /// Creates an instance T from the storyboard.
    private class func instanceFromStoryboardHelper<T: UIViewController>(storyboardName: String, storyboardBundle: Bundle, viewControllerIdentifier: String) -> T {
        
        let storyboard = UIStoryboard(name: storyboardName, bundle: storyboardBundle)
        
        return storyboard.instantiateViewController(withIdentifier: viewControllerIdentifier) as! T
    }
}
