//
//  UIView+FromNib.swift
//
//
//  Created by Philipp Otto on 03/12/2016.
//  Copyright © 2016. All rights reserved.
//

import UIKit

extension UIView {
    class func fromNib(owner: AnyObject? = nil) -> Self {
        return UIView.fromNib(owner: owner)
    }
    
    class func fromNib<T : UIView>(owner: AnyObject? = nil) -> T {
        return UIView.fromNib(withName: T.className, owner: owner) as! T
    }
    
    class func fromNib(withName name: String, owner: AnyObject? = nil) -> UIView {
        return Bundle.main.loadNibNamed(name, owner: owner, options: nil)![0] as! UIView
    }
}

// USAGE: let selectImagePickerTypeView: SelectImagePickerTypeView = .fromNib()
