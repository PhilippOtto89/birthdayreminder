//
//  BirthdayReminderUITests.swift
//  BirthdayReminderUITests
//
//  Created by Philipp Otto on 29.06.17.
//  Copyright © 2017 Philipp. All rights reserved.
//

import XCTest

class BirthdayReminderUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        continueAfterFailure = false
        
        let application = XCUIApplication()
        application.launchEnvironment = ["isUiTest":"true"]
        
        application.launch()
        
        
    }
    
    func testExample() {
        XCUIDevice.shared.orientation = .portrait
        XCUIDevice.shared.orientation = .faceUp
        
        let app = XCUIApplication()
        
        app.navigationBars["Birthdays"].buttons["Add"].tap()
        
        let nameTextField = app.textFields["Name"]
        nameTextField.tap()
        nameTextField.typeText("Philipp Otto")
        
        app.keyboards.buttons["Return"].tap()
        
        app.navigationBars.buttons["Done"].tap()
        
        let tablesQuery = app.tables
        XCTAssert(tablesQuery.cells.count == 5, "Invalid cell count after birthday creation")
        
        app.tables.children(matching: .cell).element(boundBy: 0).tap()
        
        app.buttons["Delete"].tap()
        
        XCTAssert(tablesQuery.cells.count == 4, "Invalid cell count after birthday deletion")
        

        
    }
    
}
