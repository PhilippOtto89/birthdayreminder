//
//  BirthdayTests.swift
//  BirthdayReminderTests
//
//  Created by Philipp Otto on 28.06.17.
//  Copyright © 2017 Philipp. All rights reserved.
//

import XCTest
@testable import BirthdayReminder

class BirthdayTests: XCTestCase {
    
    func testStarSign() {
        let testData: [(date: Date, expectedResult: StarSign)] = [(Date.from(year: 2017, month: 1, day: 1), .capricorn),
                                                                  (Date.from(year: 2017, month: 12, day: 21), .sagittarius),
                                                                  (Date.from(year: 2017, month: 12, day: 22), .capricorn),
                                                                  (Date.from(year: 2017, month: 2, day: 1), .aquarius),
                                                                  (Date.from(year: 2017, month: 3, day: 1), .pisces),
                                                                  (Date.from(year: 2017, month: 4, day: 1), .aries),
                                                                  (Date.from(year: 2017, month: 5, day: 1), .taurus),
                                                                  (Date.from(year: 2017, month: 6, day: 1), .gemini),
                                                                  (Date.from(year: 2017, month: 7, day: 1), .cancer),
                                                                  (Date.from(year: 2017, month: 8, day: 1), .leo),
                                                                  (Date.from(year: 2017, month: 9, day: 1), .virgo),
                                                                  (Date.from(year: 2017, month: 10, day: 1), .libra),
                                                                  (Date.from(year: 2017, month: 11, day: 1), .scorpio),
                                                                  (Date.from(year: 2017, month: 12, day: 1), .sagittarius)]
        
        for data in testData {
            let birthday = Birthday(id: UUID().uuidString, name: "Test", date: data.date, imageUrl: nil)
            
            let actualResult = birthday.starSign
            
            XCTAssert(actualResult == data.expectedResult, "\(data.date.dayMonthLongFormat) is not \(data.expectedResult.rawValue). It is \(actualResult)")
        }
    }
}


