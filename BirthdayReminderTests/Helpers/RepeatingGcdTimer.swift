//
//  RepeatingGcdTimer.swift
//  BirthdayReminderTests
//
//  Created by Philipp Otto on 28.06.17.
//  Copyright © 2017 Philipp. All rights reserved.
//

import Foundation

class RepeatingGcdTimer {
    static func get(withInterval interval: Int, queueName: String, eventHandler: @escaping () -> Void) -> DispatchSourceTimer {
        let queue = DispatchQueue(label: queueName, attributes: .concurrent)
        
        let timer = DispatchSource.makeTimerSource(queue: queue)
        
        timer.scheduleRepeating(deadline: .now(), interval: .milliseconds(interval), leeway: .seconds(1))
        
        timer.setEventHandler {
            eventHandler()
        }

        return timer
    }
}
