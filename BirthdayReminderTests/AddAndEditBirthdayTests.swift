//
//  AddAndEditBirthdayTests.swift
//  BirthdayReminderTests
//
//  Created by Philipp Otto on 28.06.17.
//  Copyright © 2017 Philipp. All rights reserved.
//

import XCTest
@testable import BirthdayReminder

class AddAndEditBirthdayTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        
        MockupModel.reset()
    }
    
    func testAddMode() {
        let presenter = AddAndEditBirthdayPresenter(model: MockupModel(), mode: .add)
        
        let view = AddAndEditBirthdayViewMockup()
        
        presenter.attachView(view)
        presenter.viewDidLoad()
        
        XCTAssert(view.showBirthdayCount == 0, "showBirthday should not have been called")
        
        let expectation = self.expectation(description: "Wait for AddAndEditBirthdayPresenter to save the birthday")
        
        presenter.saveBirthday(name: "Test", date: Date())
        
        let timer = RepeatingGcdTimer.get(withInterval: 50, queueName: "test.async.timer") {
            if view.showLoadingCount == 1 && view.hideLoadingCount == 1 && view.disposeCount == 1 {
                expectation.fulfill()
            }
        }
        
        timer.resume()
        
        self.waitForExpectations(timeout: 5.0) { (error) in
            if let error = error {
                print(error)
            }
        }
    }
    
    func testEditMode() {
        let birthday = Birthday(id: "ID1", name: "Name", date: Date(), imageUrl: nil)
        let presenter = AddAndEditBirthdayPresenter(model: MockupModel(), mode: .edit(birthday: birthday))
        
        let view = AddAndEditBirthdayViewMockup()
        
        presenter.delegate = view
        presenter.attachView(view)
        presenter.viewDidLoad()
        
        XCTAssert(view.showBirthdayCount == 1, "showBirthday should have been called once")
        
        let expectation = self.expectation(description: "Wait for AddAndEditBirthdayPresenter to save the birthday")
        
        presenter.saveBirthday(name: "Changed Name", date: Date())
        
        let timer = RepeatingGcdTimer.get(withInterval: 50, queueName: "test.async.timer") {
            
            if view.showLoadingCount == 1 &&
                view.hideLoadingCount == 1 &&
                view.disposeCount == 1 &&
                view.savedBirthday!.name == "Changed Name" {
                expectation.fulfill()
            }
        }
        
        timer.resume()
        
        self.waitForExpectations(timeout: 5.0) { (error) in
            if let error = error {
                print(error)
            }
        }
    }
}

