//
//  BirthdayListTests.swift
//  BirthdayReminderTests
//
//  Created by Philipp Otto on 25.06.17.
//  Copyright © 2017 Philipp. All rights reserved.
//

import XCTest
@testable import BirthdayReminder

class BirthdayListTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        
        MockupModel.reset()
    }
    
    func testWithFirebaseModel() {
        let listPresenter = BirthdayListPresenter(model: FirebaseModel())
        
        let expectation = self.expectation(description: "Wait for BirthdayListPresenter to load birthdays asynchronously")
        
        listPresenter.viewWillAppear()
        
        let timer = RepeatingGcdTimer.get(withInterval: 1000, queueName: "test.async.timer") {
            if !listPresenter.birthdays.isEmpty {
                expectation.fulfill()
            }
        }
        
        timer.resume()
        
        self.waitForExpectations(timeout: 10.0) { (error) in
            if let error = error {
                print(error)
            }
        }
    }
    
    func testWithMockupModel() {
        let listPresenter = BirthdayListPresenter(model: MockupModel())
        
        let expectation = self.expectation(description: "Wait for BirthdayListPresenter to load birthdays asynchronously")
        
        listPresenter.viewWillAppear()
        
        let timer = RepeatingGcdTimer.get(withInterval: 50, queueName: "test.async.timer") {
            if listPresenter.birthdays.count == 4 {
                expectation.fulfill()
            }
        }
        
        timer.resume()
        
        self.waitForExpectations(timeout: 5.0) { (error) in
            if let error = error {
                print(error)
            }
        }
    }
    
    
}
